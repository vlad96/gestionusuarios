package com.example.damalumne.aplcacionusuarios.Utils;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.damalumne.aplcacionusuarios.Activity.filtroEdad;
import com.example.damalumne.aplcacionusuarios.Database.AppDatabase;
import com.example.damalumne.aplcacionusuarios.Entity.User;

import java.util.ArrayList;
import java.util.List;

public class DatabaseInitializer {
    private static List<User> users = new ArrayList<>();
    private static final String TAG = DatabaseInitializer.class.getName();

    public static void populateAsync(@NonNull final AppDatabase db) {
        PopulateDbAsync task = new PopulateDbAsync(db);
        task.execute();
    }

    public static void populateSync(@NonNull final AppDatabase db) {
        populateWithTestData(db);
    }

    private static User addUser(final AppDatabase db, User user) {
        db.userDao().insertAll(user);
        return user;
    }

    private static void populateWithTestData(AppDatabase db) {
        User user = new User();
        user.setFirstName("Ajay");
        user.setLastName("Saini");
        user.setAge(25);
        addUser(db, user);

        users = db.userDao().getAll();
        Log.d(DatabaseInitializer.TAG, "Rows Count: " + users.size());
    }

    public void uUser(AppDatabase appDatabase, User user){

    }

    public List<User> getUser(){
        return  users;
    }

    public User findByName(AppDatabase appDatabase, String nombre, String apellido){
        return appDatabase.userDao().findByName(nombre,apellido);
    }

    public List<User> findUserByAge(AppDatabase appDatabase, int min, int max, filtroEdad filtroEdad){
        return appDatabase.userDao().findByAge(min,max);
    }

    public User findUserByName(AppDatabase appDatabase, String nombre, String apellido){
        return appDatabase.userDao().findByName(nombre,apellido);
    }

    public void crearUser(String nombre, String apellido, int edad, AppDatabase appDatabase){
        User user = new User();
        user.setFirstName(nombre);
        user.setLastName(apellido);
        user.setAge(edad);
        addUser(appDatabase, user);
    }

    public void actualizarUsuario(String nombre, String apellido, int edad, AppDatabase appDatabase){
        User user = new User();
        user.setFirstName(nombre);
        user.setLastName(apellido);
        user.setAge(edad);
        uUser(appDatabase, user);
    }

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final AppDatabase mDb;

        PopulateDbAsync(AppDatabase db) {
            mDb = db;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            populateWithTestData(mDb);
            return null;
        }

    }
}
