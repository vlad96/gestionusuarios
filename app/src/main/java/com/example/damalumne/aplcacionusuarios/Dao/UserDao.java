package com.example.damalumne.aplcacionusuarios.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.damalumne.aplcacionusuarios.Entity.User;

import java.util.List;

@Dao
public interface UserDao {
    @Query("SELECT * FROM user")
    List<User> getAll();

    @Query("SELECT * FROM user where first_name LIKE  :nombre AND last_name LIKE :apellido")
    User findByName(String nombre, String apellido);

    @Query("SELECT * FROM user WHERE age < :max AND age > :min")
    List<User> findByAge(int max, int min);

    @Query("SELECT COUNT(*) from user")
    int countUsers();

    @Insert
    void insertAll(User... users);

    @Delete
    void delete(User user);

    @Update
    void updateUser(User user);


}
