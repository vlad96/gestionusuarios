package com.example.damalumne.aplcacionusuarios.Activity;


import android.app.AppComponentFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.damalumne.aplcacionusuarios.Database.AppDatabase;
import com.example.damalumne.aplcacionusuarios.Entity.User;
import com.example.damalumne.aplcacionusuarios.R;
import com.example.damalumne.aplcacionusuarios.Utils.DatabaseInitializer;

import java.util.ArrayList;
import java.util.List;

public class filtroNombre extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerAdapter;
    private List<User> users = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.v_filtro_nombre);

        DatabaseInitializer databaseInitializer = new DatabaseInitializer();

        recyclerView = (RecyclerView)findViewById(R.id.recy1);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        users.add(databaseInitializer.findUserByName((AppDatabase.getAppDatabase(this)), getIntent().getStringExtra("nombre"), getIntent().getStringExtra("apellido")));

        recyclerAdapter = new Adaptacion(users, this);
        recyclerView.setAdapter(recyclerAdapter);
    }
}
