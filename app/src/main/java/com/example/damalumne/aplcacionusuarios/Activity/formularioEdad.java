package com.example.damalumne.aplcacionusuarios.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.damalumne.aplcacionusuarios.R;

public class formularioEdad extends AppCompatActivity implements View.OnClickListener {

    private EditText min;
    private EditText max;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filtro_edad);

        min = findViewById(R.id.min_edad);
        max = findViewById(R.id.max_edad);

        Button b = (Button)findViewById(R.id.filtrar_edad);
        b.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(this, filtroEdad.class);
        i.putExtra("min", Integer.parseInt(min.getText().toString()));
        i.putExtra("max", Integer.parseInt(max.getText().toString()));
        startActivity(i);
    }
}
