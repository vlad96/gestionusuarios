package com.example.damalumne.aplcacionusuarios.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.damalumne.aplcacionusuarios.Database.AppDatabase;
import com.example.damalumne.aplcacionusuarios.R;
import com.example.damalumne.aplcacionusuarios.Utils.DatabaseInitializer;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button filtroUser = (Button)findViewById(R.id.listar_existentes);
        filtroUser.setOnClickListener(this);

        Button filtroNombre = (Button)findViewById(R.id.filtrar_nombre);
        filtroNombre.setOnClickListener(this);

        Button filtroEdad = (Button)findViewById(R.id.filtrar_edad);
        filtroEdad.setOnClickListener(this);

        Button addUser = (Button)findViewById(R.id.add_user);
        addUser.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        AppDatabase.destroyInstance();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        DatabaseInitializer.populateAsync(AppDatabase.getAppDatabase(this));

        if(v.getId() == R.id.listar_existentes){
            Intent i = new Intent(this, listUser.class);
            startActivity(i);
        }

        if(v.getId() == R.id.filtrar_nombre){
            Intent i = new Intent(this, formularioNombre.class);
            startActivity(i);
        }

        if(v.getId() == R.id.filtrar_edad){
            Intent i = new Intent(this, formularioEdad.class);
            startActivity(i);
        }

        if(v.getId() == R.id.add_user){
            Intent i = new Intent(this, addUser.class);
            startActivity(i);
        }
    }
}
