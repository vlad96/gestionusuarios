package com.example.damalumne.aplcacionusuarios.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.damalumne.aplcacionusuarios.Database.AppDatabase;
import com.example.damalumne.aplcacionusuarios.R;
import com.example.damalumne.aplcacionusuarios.Utils.DatabaseInitializer;

public class listUser extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.v_list_user);

        DatabaseInitializer databaseInitializer = new DatabaseInitializer();
        recyclerView = (RecyclerView)findViewById(R.id.recy4);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerAdapter = new Adaptacion(databaseInitializer.getUser(), this);
        recyclerView.setAdapter(recyclerAdapter);
    }
}
