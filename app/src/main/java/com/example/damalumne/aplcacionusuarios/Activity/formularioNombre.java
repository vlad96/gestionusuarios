package com.example.damalumne.aplcacionusuarios.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.damalumne.aplcacionusuarios.R;

public class formularioNombre extends AppCompatActivity implements View.OnClickListener {

    private EditText nombre;
    private EditText apellido;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filtro_nombre);

        nombre = findViewById(R.id.nombreFiltro);
        apellido = findViewById(R.id.apellidoFiltro);

        Button b = (Button)findViewById(R.id.buscar);
        b.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(this, filtroNombre.class);
        i.putExtra("nombre", nombre.getText().toString());
        i.putExtra("apellido", apellido.getText().toString());
        startActivity(i);
    }
}
