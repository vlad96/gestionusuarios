package com.example.damalumne.aplcacionusuarios.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.damalumne.aplcacionusuarios.Database.AppDatabase;
import com.example.damalumne.aplcacionusuarios.Entity.User;
import com.example.damalumne.aplcacionusuarios.R;

public class updateUser extends AppCompatActivity implements View.OnClickListener {
    public EditText nombre;
    public EditText apellido;
    public EditText edad;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actualizar_user);

        Button b = (Button)findViewById(R.id.bActu);
        b.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        nombre = findViewById(R.id.editNombre);
        apellido = findViewById(R.id.editApellido);
        edad = findViewById(R.id.editEdad);
        User user = new User();

        user = (User)getIntent().getSerializableExtra("updateUser");
        user.setFirstName(nombre.getText().toString());
        user.setLastName(apellido.getText().toString());
        AppDatabase.getAppDatabase(this).userDao().updateUser(user);
    }
}
