package com.example.damalumne.aplcacionusuarios.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.damalumne.aplcacionusuarios.Database.AppDatabase;
import com.example.damalumne.aplcacionusuarios.R;
import com.example.damalumne.aplcacionusuarios.Utils.DatabaseInitializer;

public class addUser extends AppCompatActivity implements View.OnClickListener {

    public EditText nombre;
    public EditText apellido;
    public EditText edad;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_user);

        Button b = (Button)findViewById(R.id.bCrear);
        b.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        nombre = findViewById(R.id.addNombre);
        apellido = findViewById(R.id.addApellido);
        edad = findViewById(R.id.addEdad);

        DatabaseInitializer databaseInitializer = new DatabaseInitializer();

        databaseInitializer.crearUser(nombre.getText().toString(), apellido.getText().toString(), Integer.parseInt(edad.getText().toString()),(AppDatabase.getAppDatabase(this)));
        this.finish();
    }
}
