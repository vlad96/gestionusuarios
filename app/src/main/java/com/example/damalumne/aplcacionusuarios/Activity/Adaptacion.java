package com.example.damalumne.aplcacionusuarios.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.damalumne.aplcacionusuarios.Database.AppDatabase;
import com.example.damalumne.aplcacionusuarios.Entity.User;
import com.example.damalumne.aplcacionusuarios.R;

import java.util.List;

public class Adaptacion extends RecyclerView.Adapter<Adaptacion.Usuario> {

    private List<User> users;
    private Context context;

    public Adaptacion(List<User> users, Context context){
        this.context = context;
        this.users = users;
    }

    public Adaptacion(List<User> userByAge) {
    }

    @NonNull
    @Override
    public Usuario onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.usuario, viewGroup, false);
        return new Usuario(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Usuario usuario, int i) {
        usuario.idd.setText(String.valueOf(users.get(i).getUid()));
        usuario.nombre.setText(users.get(i).getFirstName());
        usuario.apellido.setText(users.get(i).getLastName());
        usuario.edad.setText(String.valueOf(users.get(i).getAge()));
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class Usuario extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView idd;
        public TextView nombre;
        public TextView apellido;
        public TextView edad;
        public Button bUpdate;
        public Button bDelete;

        public Usuario(View v){
            super(v);

            idd = (TextView)v.findViewById(R.id.idText);
            nombre = (TextView)v.findViewById(R.id.nomText);
            apellido = (TextView)v.findViewById(R.id.apeText);
            edad = (TextView)v.findViewById(R.id.edadText);

            bUpdate = (Button)v.findViewById(R.id.bUpdate);
            bDelete = (Button)v.findViewById(R.id.bDelete);

            bUpdate.setOnClickListener(this);
            bDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v){
            if(v.getId() == R.id.bUpdate){
                Intent i = new Intent(context, updateUser.class);
                i.putExtra("updateUser", String.valueOf(users.get(getAdapterPosition())));
                context.startActivity(i);
            }else if(v.getId() == R.id.bDelete){
                AppDatabase.getAppDatabase(context).userDao().delete(users.get(getAdapterPosition()));
            }
        }

    }
}
